import React, {
  createRef,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import SplitPaneContext from "./splitPaneContext";
import Button from '@mui/material/Button';
import Progressbar from '../components/progressbar';


const SplitPane = ({ children, ...props }) => {
  const [clientHeight, setClientHeight] = useState(null);
  const [clientWidth, setClientWidth] = useState(null);
  const yDividerPos = useRef(null);
  const xDividerPos = useRef(null);

  const onMouseHoldDown = (e) => {
    yDividerPos.current = e.clientY;
    xDividerPos.current = e.clientX;
  };

  const onMouseHoldUp = () => {
    yDividerPos.current = null;
    xDividerPos.current = null;
  };

  const onMouseHoldMove = (e) => {
    if (!yDividerPos.current && !xDividerPos.current) {
      return;
    }

    setClientHeight(clientHeight + e.clientY - yDividerPos.current);
    setClientWidth(clientWidth + e.clientX - xDividerPos.current);

    yDividerPos.current = e.clientY;
    xDividerPos.current = e.clientX;
  };

  useEffect(() => {
    document.addEventListener("mouseup", onMouseHoldUp);
    document.addEventListener("mousemove", onMouseHoldMove);

    return () => {
      document.removeEventListener("mouseup", onMouseHoldUp);
      document.removeEventListener("mousemove", onMouseHoldMove);
    };
  });

  return (
    <div {...props}>
      <SplitPaneContext.Provider
        value={{
          clientHeight,
          setClientHeight,
          clientWidth,
          setClientWidth,
          onMouseHoldDown,
        }}
      >
        {children}
      </SplitPaneContext.Provider>
    </div>
  );
};

export const Divider = (props) => {
  const { onMouseHoldDown } = useContext(SplitPaneContext);

  return <div {...props} onMouseDown={onMouseHoldDown} />;
};

export const SplitPaneTop = (props) => {
  const topRef = createRef();
  const { clientHeight, setClientHeight } = useContext(SplitPaneContext);
  const projectList = ["Project1", "Project2", "Project3"]

  useEffect(() => {
    if (!clientHeight) {
      setClientHeight(topRef.current.clientHeight);
      return;
    }

    topRef.current.style.minHeight = clientHeight + "px";
    topRef.current.style.maxHeight = clientHeight + "px";
  }, [clientHeight]);

  return (
    <div {...props} className="split-pane-top" ref={topRef}>
      <h1>Projects:</h1>
      <ul>
        {projectList.map((el, i) => {
          return (
            <li key={i}>
              <a href="#" onClick={() => el}>
                {el}
              </a>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export const SplitPaneRTop = (props) => {
    const topRef = createRef();
    const { clientHeight, setClientHeight } = useContext(SplitPaneContext);
    const projectList = ["Project1", "Project2", "Project3"]
  
    useEffect(() => {
      if (!clientHeight) {
        setClientHeight(topRef.current.clientHeight);
        return;
      }
  
      topRef.current.style.minHeight = clientHeight + "px";
      topRef.current.style.maxHeight = clientHeight + "px";
    }, [clientHeight]);
  
    return (
      <div {...props} className="split-pane-top" ref={topRef}>
        <h1>Auotmation Details:</h1>
        <ul>
          {projectList.map((el, i) => {
            return (
                <div> Automation Details </div>
            );
          })}
        </ul>
      </div>
    );
  };

export const SplitPaneBottom = (props) => {

  return (
    <div {...props} className="split-pane-bottom">
        <b>Project Details : </b>
         <div> Project1 Details </div>
    </div>
  );
};



export const SplitPaneRBottom = (props) => {

    return (
      <div {...props} className="split-pane-bottom">
          <Button variant="contained" onClick={() => this.nextPrev("next")}>
            Run
          </Button>

            <div className="title">Test Case Progess:</div>
            <Progressbar bgcolor="#99ccff" progress='50'  height={30} />

            <Button variant="contained" onClick={event =>  window.location.href='/dashboard'}>
            Go to Results Dashboard
          </Button>
      </div>
      
    );
  };

export const SplitPaneLeft = (props) => {
  const topRef = createRef();
  const { clientWidth, setClientWidth } = useContext(SplitPaneContext);

  useEffect(() => {
    if (!clientWidth) {
      setClientWidth(topRef.current.clientWidth / 2);
      return;
    }
    topRef.current.style.minWidth = clientWidth + "px";
    topRef.current.style.maxWidth = clientWidth + "px";
  }, [clientWidth]);

  return <div {...props} className="split-pane-left" ref={topRef} />;
};

export const SplitPaneRight = (props) => {

  const [checked, setChecked] = useState([]);
  const checkList = ["Test Case 1", "Test Case 2", "Test Case 3", "Test Case 4"];

  const handleCheck = (event) => {
    var updatedList = [...checked];
    if (event.target.checked) {
      updatedList = [...checked, event.target.value];
    } else {
      updatedList.splice(checked.indexOf(event.target.value), 1);
    }
    setChecked(updatedList);
  };

  const checkedItems = checked.length
    ? checked.reduce((total, item) => {
        return total + ", " + item;
      })
    : "";

  var isChecked = (item) =>
    checked.includes(item) ? "checked-item" : "not-checked-item";

  return (
    <div {...props} className="split-pane-right">
        <div className="checkList">
            <div className="title">Test Cases:</div>
                <div className="list-container">
                {checkList.map((item, index) => (
                    <div key={index}>
                    <input value={item} type="checkbox" onChange={handleCheck} />
                    <span className={isChecked(item)}>{item}</span>
                    </div>
                ))}
                </div>
            </div>
        <div>
            {`Selected Test Cases : ${checkedItems}`}
        </div>
    </div>
  );
};

export const SplitPaneExtremeRight = (props) => {

    return (
      <div {...props} className="split-pane-exright">
   
      </div>
    );
  };

export default SplitPane;
